# Lab 5: Looping Patterns and Hurricane API

Let us start Lab 5! This lab introduces you to some fundamental looping patterns that will help solve P5. It is
designed to help you become comfortable using the functions in `project.py`. You will also
learn basic methods to manipulate strings needed for P5.

## Corrections and clarifications

None yet.

**Find any issues?** Let Jane or Adi know during lab, or create a post on Piazza.

----------------------------------
## Learning Objectives:
In this lab you will practice:
- inspecting the `project.py` file,
- iterating through data using a `for` loop,
- writing algorithms to search, filter, count, and find min / max,
- writing algorithms that store/use indices
- writing helper functions,
- writing algorithms that manipulate strings.
----------------------------------
## Introduction:
In this lab, you will look at hurricane data and learn techniques to extract specific data. Data
scientists use such data when studying the effects of climate change.

According to the [Center for Climate and Energy Solutions](https://www.c2es.org/content/hurricanes-and-climate-change/), "Climate change is worsening
hurricane impacts in the United States by increasing the intensity and decreasing the speed at
which they travel. Scientists are currently uncertain whether there will be a change in the number
of hurricanes, but they are certain that the intensity and severity of hurricanes will continue to
increase. These trends make hurricanes far more costly in terms of physical damage and deaths."
By tracking past hurricanes' speed, number of fatalities, and property damage, scientists can prepare for
future ones.

------------------------------

## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start working on Lab 5 with one person, then do the project with a different partner.  Now may be a good time to review [our course policies](https://canvas.wisc.edu/courses/355767/pages/syllabus?module_item_id=6048035).

------------------------------

## Project partner

We strongly recommend students find a project partner. Pair programming is a great way to learn from a fellow student. Project difficulty increases exponentially in this course. Finding a project partner early on during the semester is a good idea.

If you are still looking for a project partner, take a moment now to ask around the room if anyone would like to partner with you on this project. Then you can work with them on this lab as well as the project.

----------------------------------

## Segment 1: Setup

Create a `lab5` directory and download the following files into the `lab5` directory:

* `hurricanes.csv`
* `project.py`
* `practice.ipynb`
* `practice_test.py`

Once you have downloaded the files, open a terminal and navigate to your `lab5` directory.
Run `ls` to make sure the above files are available.

**Note:** If you accidentally downloaded the file as a `.txt` instead of `.csv` (or `.cvs` or `.csv.txt`)
(say `hurricanes.cvs`), you can execute `mv hurricanes.cvs hurricanes.csv` on a
Terminal/PowerShell window. Recall that the `mv` (move) command lets you rename a source file
(first argument, example: `hurricanes.cvs`) to the destination file (second argument, example:
`hurricanes.csv`).

----------------------------------
## Segment 2: Learning the API

You will be finishing the rest of your lab on `practice.ipynb`. Run the command `jupyter notebook` from your Terminal/PowerShell window.
Remember not to close this
Terminal/PowerShell window while Jupyter is running, and open a new Terminal/PowerShell
window if necessary.

**Note**: For P5, you will be working on `p5.ipynb`, which is very similar to `practice.ipynb`.
It is strongly recommended that you finish this notebook before moving on to P5,
so you can ask your TA/PM any questions about the notebook that may arise.

**Note**: Unlike `p5.ipynb`, you do **not** have to submit `practice.ipynb`. This notebook is solely
for your practice and preparation for P5.

------------------------------

You can now get started with [P5](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-lecture-material/-/tree/main/sum23/projects/p5). **You may use any helper functions created here in project p5**. Remember to only work with P5 with your partner from this point on. Have fun!
